﻿using System;
using Accord.Math.Optimization;

namespace  Accord.Statistics.Models.Regression.Fitting
{
    public class NonlinearLeastSquares_ext : Accord.Statistics.Models.Regression.Fitting.NonlinearLeastSquares
    {
        public NonlinearRegression Learn<T>(Double[][] input, double[] output) where T:BaseOptimizationMethod
        {
            NonlinearRegression problemFormulation = null;

            Func<double[][],double[],double[], double> error = ErrorFunction;
             var errorForInputAndOutput = error.Curry()(input)(output);
          
            var gradientFreeSolver = (BaseOptimizationMethod) Activator.CreateInstance(typeof(T), this.NumberOfParameters );
            gradientFreeSolver.Function = errorForInputAndOutput;
            gradientFreeSolver.NumberOfVariables = this.NumberOfParameters;
            
            bool success = gradientFreeSolver.Minimize();

            problemFormulation = new NonlinearRegression(gradientFreeSolver.NumberOfVariables,this.Function);

            for(int idx = 0; idx < gradientFreeSolver.Solution.Length;idx++)
            {
                problemFormulation.Coefficients.SetValue(gradientFreeSolver.Solution[idx],idx);
            }

            return problemFormulation;
        }

        protected double ErrorFunction(double[][] input, double[] output, double[]parameter)
        {
            int points = input.Length;
            double error = 0;
            // invoke the function to compute the regression functions output 
            // build the difference between computed output and true output, i.e. error
            // error with power of 2 and sum them up
            for (int idx = 0; idx < points; idx++)
            {
                error =  error + System.Math.Pow(Function.Invoke(parameter,input[idx]) - output[idx],2);
            }
            return error;
        }
        
    }
    // magic transformation to map a 4 parameter function to partial application
    // like in F# 
    internal static class CurryExtension
    {
        internal static Func<A,Func<B,Func<C,D>>> Curry<A,B,C,D>(this Func<A,B,C,D> func) => a => b => c => func(a,b,c);
        internal static Func<A,Func<B,C>> Curry<A,B,C>(this Func<A,B,C> func) => a => b => func(a,b);
    }
}
