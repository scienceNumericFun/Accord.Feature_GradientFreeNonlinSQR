using System;
using Xunit;
using Accord.Statistics.Models.Regression.Fitting;
using Accord.Math.Optimization;

namespace Fitting.Test
{
    public class NonLinsqrtTester
    {
        protected NonlinearLeastSquares_ext _nonlinLSQ_linFcn;
        protected NonlinearLeastSquares_ext _nonlinLSQ_QuadratFcn;
        public NonLinsqrtTester()
        {
            _nonlinLSQ_linFcn = new NonlinearLeastSquares_ext();
       }
        [Fact]
        public void CheckLinCoeff()
        {
            var input = new double[][]{new double[]{0},new double[]{1}};
            var output = new double[] {3,5};

            _nonlinLSQ_linFcn.NumberOfParameters = 2;
            _nonlinLSQ_linFcn.Function = (w,x) => x[0] * w[0] + w[1];
            var regressionProb = _nonlinLSQ_linFcn.Learn<NelderMead>(input,output);
        }
    }
}
